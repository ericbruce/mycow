package com.sinosoft.mq.mail;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class MqMailSender {
	JavaMailSender  mailSender;
	SimpleMailMessage mailMessage;
	private final static  Logger LOG = LoggerFactory.getLogger(MqMailSender.class);
	public static boolean sendedUdp=false;
	public static boolean sendedSorted=false;
	public static boolean sendedUpMq=false;

	public JavaMailSender getMailSender() {
		return mailSender;
	}
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	public SimpleMailMessage getMailMessage() {
		return mailMessage;
	}
	public void setMailMessage(SimpleMailMessage mailMessage) {
		this.mailMessage = mailMessage;
	}

	public MqMailSender(){
		AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext-msg.xml");
		mailSender = (JavaMailSender)ctx.getBean("mailSender");
		mailMessage = (SimpleMailMessage)ctx.getBean("mailMessage");
	}
	/**邮件内容发送
	 * @param email
	 * @param msg
	 * @return int 1 - 发送成功； 0 - 发送失败
	 */
	public int msgSend(String emailAddr, String msg){
		int ret = 0;	//发送成功或失败标志
//		mailMessage.setSubject("邮件发送");
		try {
			mailMessage.setSentDate(new Date());		//发送时间（当前时间）
			mailMessage.setTo(emailAddr);				//接收者
			mailMessage.setText(msg);					//邮件内容
			mailSender.send(mailMessage);				//发送
			ret = 1;//成功
		} catch (Exception e){
			e.printStackTrace();
			ret = 0;//失败
		}
		return ret;
	}

	 public  void sendUdp(Exception e,String nodeId){
		 if(!sendedUdp){
			 StringBuilder sb=new StringBuilder();
			 sb.append("upd连接出错，当前标识:"+nodeId+"\n");
			 StackTraceElement[] tracs=e.getStackTrace();
			 for(StackTraceElement element:tracs){
				 sb.append(element.toString()).append("\n");
			 }
			 mailMessage.setText(sb.toString()); 
			 mailSender.send(getMailMessage());
			 sendedUdp=true;
		 }else{
			 LOG.info("已经发送过邮件！");
		 }
	 }
	 public  void sendSort(Exception e,String nodeId){
		 if(!sendedSorted){
			 StringBuilder sb=new StringBuilder();
			 sb.append("sortting连接出错，当前标识:"+nodeId+"\n");
			 StackTraceElement[] tracs=e.getStackTrace();
			 for(StackTraceElement element:tracs){
				 sb.append(element.toString()).append("/n");
			 }
			 mailMessage.setText(sb.toString()); 
			 mailSender.send(mailMessage);
			 sendedSorted=true;
		 }else{
			 LOG.info("已经发送过邮件！");
		 }
		 
	 }
	 public  void sendUpMq(Exception e,String nodeId){
		 if(!sendedUpMq){
			 StringBuilder sb=new StringBuilder();
			 sb.append("连接上级mq连接出错，当前标识:"+nodeId+"\n");
			 StackTraceElement[] tracs=e.getStackTrace();
			 for(StackTraceElement element:tracs){
				 sb.append(element.toString()).append("/n");
			 }
			 mailMessage.setText(sb.toString()); 
			 mailSender.send(mailMessage);
			 sendedUpMq=true;
		 }else{
			 LOG.info("已经发送过邮件！");
		 }
		 
	 }
	
}
