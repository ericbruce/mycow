package com.sinosoft.mq.sms;

/**
 * 短信类
 * @author eric
 *
 */
public class SmsInfo {
	private String userid;		//账户
	private String password;	//密码
	private String url;			//请求链接URL
	private String content;		//信息内容()
	private String telphone;	//发送号码
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
