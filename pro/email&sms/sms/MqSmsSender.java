package com.sinosoft.mq.sms;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * 短信发送类
 * @author eric
 *
 */
public class MqSmsSender {
	Logger logger = Logger.getLogger(MqSmsSender.class);
	SmsInfo smsMessage;
	
	public MqSmsSender(){
		AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext-msg.xml");
		smsMessage = (SmsInfo)ctx.getBean("smsMessage");
	}
	
	/**生成请求链接
	 * @param telphone	发送号码
	 * @param content	短信内容
	 * @return 请求链接
	 */
	private String getRequestUrl(String telphone, String content){
		return smsMessage.getUrl() 
				+ "?sendNumber=" + telphone 
				+ "&content=" + content 
				+ "&userid=" + smsMessage.getUserid() 
				+ "&password=" + smsMessage.getPassword();
	}
	
	/**短信发送
	 * @param telphone
	 * @param content
	 * @return 链接请求返回值
	 */
	public String send(String telphone, String content){
		String returnStr = null;		//返回值
		String urlStr = getRequestUrl(telphone, content);		//
		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();
			 // 设置通用的请求属性
			urlConn.setRequestProperty("accept", "*/*");
			urlConn.setRequestProperty("connection", "Keep-Alive");
			urlConn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			 // 建立实际的连接
			urlConn.connect();
			InputStream is = urlConn.getInputStream();
			byte[] b = new byte[1];
			is.read(b);
			returnStr = new String(b);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnStr;
	}
}
