﻿hojo.provide("icallcenter.logon");

icallcenter.logon.startLogon = function (loginName, password, extenType) {
    //var url = "http://a3.7x24cc.com/commonInte/";
	var url = "http://a3.7x24cc.com/edb/phoneBar/phonebar.html";
    hojo.io.script.get({
        url: url,
        content: { flag: "7", password: password, extenType: extenType, loginName: loginName },
        timeout: 15000,
        callbackParamName: "callbackName",
        preventCache: true,
        load: function (response, ioArgs) {

            if (response.resultCode == "0000") {
                var _result = response.result;
                var config = {
                    sipNo: _result.extension,
                    Monitor: _result.monitor,
                    accountId: _result.accountId,
                    pbx_in_ip: _result.pbx_in_ip,
                    proxy_url: _result.proxy_url,
                    userId: _result.userId,
                    extenType: extenType,
                    password: password
                };
                icallcenter.logon.initPhone(config);
            } else {
                alert("账号或密码密码错误！！"); 
            }

        },
        error: function (response, ioArgs) {
            alert("呼叫中心用户注销返回错误");
        }
    });
};

//init softphone
icallcenter.logon.initPhone = function (config) {
    hojo.require("icallcenter.Phone");
    icallcenter.Phone.registerEvent(config);
};

icallcenter.logon.afterPhone = function (phone) {
    if (phone) {
        hojo.require("icallcenter.SoftphoneBar");
        softphoneBar = new icallcenter.SoftphoneBar(phone, "softphonebar");
        hojo.require("icallcenter.callProcessor");
        callProcessor = new icallcenter.callProcessor(phone);
    }
};

icallcenter.logon.getUrlValue = function (param) {
    var query = window.location.search;
    var iLen = param.length;
    var iStart = query.indexOf(param);
    if (iStart == -1)
        return "";
    iStart += iLen + 1;
    var iEnd = query.indexOf("&", iStart);
    if (iEnd == -1)
        return query.substring(iStart);
    return query.substring(iStart, iEnd);
};


//登出
icallcenter.logon.logout = function(){
    if(phone) {
		phone.destroy(true);
	}
};