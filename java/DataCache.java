package com.sinosoft.ie.job;

import java.rmi.RemoteException;
import java.util.Calendar;

import javax.xml.rpc.ServiceException;

import org.tempuri.AuthService;
import org.tempuri.AuthServiceLocator;
import org.tempuri.AuthServiceSoap;

/** 
 * @author yx 
 * @version 创建时间：2012-11-8 上午10:49:50 
 * 类说明：webservice数据定时采集 2012-01-01-今天 
 */
public class DataCache {
	//上传时间是否已加载
	public static boolean isLoaded = false;
	//上次上传时间
	public static String lastAddTime;
	//血糖设备类型
	public static String BgSensor = "HC601";
	//血压设备类型
	public static String BpSensor = "HC502";
	//心率设备类型
	public static String ecgSensor = "";
	//血氧设备类型
	public static String spo2Sensor = "";
	//计步设备类型
	public static String stepSensor = "HC901";
	
	public static String getToken(){
		AuthService as=new AuthServiceLocator();
		String token = null;
		AuthServiceSoap ass;
		try {
			ass = as.getAuthServiceSoap();
			token = ass.authClient("zgcws", "5D1C453DA0D2B0CEAB949BABA389CB8E");
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return token;
	}
}
