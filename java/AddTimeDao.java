/**
 * 
 */
package com.sinosoft.ie.dao.ytk;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.springframework.stereotype.Repository;

import com.sinosoft.ie.job.DataCache;

/**
 * 说明：
 * @author yx
 * 创建时间：2012-12-21下午1:52:06
 */
@Repository
public class AddTimeDao {
	//上次上传时间
	private String lastAddTime;
	
	public void setLastAddTime(String lastAddTime) {
		this.lastAddTime = lastAddTime;
	}

	//获取上次上传时间
	public Calendar findLastAddTime(){
		try{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (!DataCache.isLoaded){								//还没加载上次的上传时间
			DataCache.lastAddTime = getAddTimeFromProFile();
			DataCache.isLoaded = true;
		}
		Date date = sdf.parse(DataCache.lastAddTime);
		Calendar fromDate = Calendar.getInstance();
		fromDate.setTime(date);
		return fromDate;										//
		} catch (ParseException e){
			e.printStackTrace();
			return null;
		}
	}
	
	private String getAddTimeFromProFile(){
		String lastAddTime = null;
		try {
			Properties prop = new Properties();// 属性集合对象 
			URL url = this.getClass().getClassLoader().getResource("");
			FileInputStream fis = new FileInputStream(url.getPath().replace("/classes/","/lastAddTime.properties"));// 属性文件输入流 
			prop.load(fis);// 将属性文件流装载到Properties对象中 
			lastAddTime = prop.getProperty("lastAddTime");
			fis.close();// 关闭流
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		return lastAddTime;
	}
	
	//更新上次上传时间
	public int updateLastAddTime(Calendar toDate){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		URL url = this.getClass().getClassLoader().getResource("");
		String location = url.getPath();
		String lastAddTime = sdf.format(toDate.getTime());
		Properties prop = new Properties();// 属性集合对象 
		prop.setProperty("lastAddTime", lastAddTime);
		DataCache.lastAddTime = lastAddTime;
		try{
			FileOutputStream fos = new FileOutputStream(location.replace("/classes/", "/lastAddTime.properties")) ;
			prop.store(fos, "last add time");
			return 0;
		} catch (FileNotFoundException e){
			e.printStackTrace();
			return -1;
		} catch (IOException e){
			e.printStackTrace();
			return -1;
		}
	}
}
