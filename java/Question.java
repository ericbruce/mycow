/**
 * 
 */
package com.sinosoft.ie.services.health;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 说明：
 * @author yx
 * 创建时间：2013-2-6上午11:45:22
 */
public class Question {
	//属性
	private int Id;								//ID
	private String desc;						//描述
	private boolean leaf;						//判断是否为树叶
	private List<Question> questions;			//子题目
	private double questionweight;				//问题权值
	private int questionPid;					//问题父ID
	private String questionType;				//题型
	private int questionno;						//题号
	private int optionvalue;					//选项值
	
	public Question(){
		super();
		this.Id = 0;					//为0表示没对该对象id做任何操作(即还没获取到ID从数据库中)
		this.questions = null;
	}
	
	/**
	 * @param desc
	 * @param isLeaf
	 * @param questions
	 * @param questionweight
	 * @param questionPid
	 * @param questionType
	 * @param questionno
	 * @param optionvalue
	 */
	public Question(String desc, boolean isLeaf, List<Question> questions,
			double questionweight, int questionPid, String questionType,
			int questionno, int optionvalue) {
		super();
		this.desc = desc;
		this.leaf = isLeaf;
		this.questions = questions;
		this.questionweight = questionweight;
		this.questionPid = questionPid;
		this.questionType = questionType;
		this.questionno = questionno;
		this.optionvalue = optionvalue;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return Id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		Id = id;
	}
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * @return the isLeaf
	 */
	public boolean isLeaf() {
		return leaf;
	}
	/**
	 * @param isLeaf the isLeaf to set
	 */
	public void setLeaf(boolean isLeaf) {
		this.leaf = isLeaf;
	}
	/**
	 * @return the questions
	 */
	public List<Question> getQuestions() {
		return questions;
	}
	/**
	 * @param questions the questions to set
	 */
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	/**
	 * @return the questionweight
	 */
	public double getQuestionweight() {
		return questionweight;
	}
	/**
	 * @param questionweight the questionweight to set
	 */
	public void setQuestionweight(double questionweight) {
		this.questionweight = questionweight;
	}
	/**
	 * @return the questionPid
	 */
	public int getQuestionPid() {
		return questionPid;
	}
	/**
	 * @param questionPid the questionPid to set
	 */
	public void setQuestionPid(int questionPid) {
		this.questionPid = questionPid;
	}
	/**
	 * @return the questionType
	 */
	public String getQuestionType() {
		return questionType;
	}
	/**
	 * @param questionType the questionType to set
	 */
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	/**
	 * @return the questionno
	 */
	public int getQuestionno() {
		return questionno;
	}
	/**
	 * @param questionno the questionno to set
	 */
	public void setQuestionno(int questionno) {
		this.questionno = questionno;
	}
	/**
	 * @return the optionvalue
	 */
	public int getOptionvalue() {
		return optionvalue;
	}
	/**
	 * @param optionvalue the optionvalue to set
	 */
	public void setOptionvalue(int optionvalue) {
		this.optionvalue = optionvalue;
	}
	
	public static List<Question> questionEncapsulation(List<Map<String,Object>> tquestions){
		List<Question> questions = new ArrayList<Question>();											//存放处理后的题目
		List<Question> reQuestions = new ArrayList<Question>();											//存放剩下（尚未装入）的
		for (int i = 0; i < tquestions.size(); i++){	//题目 的封装
			Question question = new Question();
			question.setId(Integer.parseInt(tquestions.get(i).get("questionid").toString()));
			Object questionpid = tquestions.get(i).get("questionpid");		//父ID
			int pid = -1;
			if (questionpid != null){
				pid = Integer.parseInt(questionpid.toString());
				question.setQuestionPid(pid);
			}
			Object questiondesc = tquestions.get(i).get("questiondesc");	//题目描述
			if (questiondesc != null)
				question.setDesc(questiondesc.toString());
			Object questiontype = tquestions.get(i).get("questiontype");	//题型
			if (questiontype != null){
				question.setQuestionType(questiontype.toString());
			}
			Object questionweight = tquestions.get(i).get("questionweight");//题目权值
			if (questionweight != null){
				question.setQuestionweight(Double.parseDouble(questionweight.toString()));
			}
			Object questionno = tquestions.get(i).get("questionno");		//题号
			if (questionno != null){
				question.setQuestionno(Integer.parseInt(questionno.toString()));
			}
			question.setLeaf(false);	//不是叶结点
			if (pid == -1){				//根节点
				questions.add(question);
			} else {					//中间节点
				reQuestions.add(question);
			}
		}
		questions = linkNodes(questions, reQuestions);					//插入节点
		return questions;
	}
	/**
	 * 将中间子节点连接在父节点上
	 * @param questions
	 * @param reQuestions
	 */
	public static List<Question> linkNodes(List<Question> questions, List<Question> reQuestions){
//		List<Question> rQuestions = new ArrayList<Question>();		//返回的森林
//		rQuestions = questions;
		for (int i = 0; i < reQuestions.size(); i++){
			Question fromquestion = reQuestions.get(i);				//要插入的节点
			int pid = fromquestion.getQuestionPid();				//节点父ID
			for (int j = 0; j < questions.size(); j++){
				Question toquestion = questions.get(j);			//一颗树
				if (toquestion.getId() == pid){
					if (toquestion.getQuestions() == null){		//第一个子节点
						List<Question> qs = new ArrayList<Question>();		//创建新子节点
						qs.add(fromquestion);								//添加(插入)新子节点
						toquestion.setQuestions(qs);			//修改树
					} else {
						List<Question> qs = toquestion.getQuestions();
						qs.add(fromquestion);
						toquestion.setQuestions(qs);
					}
//					rQuestions.add(toquestion);					
//					rQuestions.set(j, toquestion);			//替换之前的节点(新节点已插入子节点)
					questions.set(j, toquestion);			//替换之前的节点(新节点已插入子节点)
					break;
				}
				
			}
		}
		return questions;
	}
}
