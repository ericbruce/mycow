/**
 * 通用的excel导出
 */
package com.sinosoft.ie.excel;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.Region;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sinosoft.ie.dao.resident.ResidentInfoDao;

/**
 * 说明：
 * @author yx
 * 创建时间：2012-12-27上午9:54:29
 */
@Service
@Transactional
public class CommonExcelExport {
	@Resource
	ResidentInfoDao residentInfoDao;
	
		
	public void excelExport(HttpServletResponse response,
							java.util.List<String> cellName,								//列标题
							java.util.List<Map<Integer,Object>> cellValues,					//列值
							String workBookName){											//工作簿名称
		OutputStream out=null;
		//获取居民信息
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();
		HSSFRow row = sheet.createRow((int) 0);
		HSSFCell cell;	
		for (int i = 0; i < cellName.size(); i++){											//列标题填充
			cell = row.createCell(i);
			cell.setCellValue(cellName.get(i));
		}		
		for (int j = 0; j < cellValues.size(); j++){											//单元格值填充
			row = sheet.createRow(j + 1);
			Map<Integer, Object>ret = cellValues.get(j);
			for (int z = 0; z < cellName.size(); z++){
				Object value = ret.get(z);
				String cellValue = "";
				if (value != null)
					cellValue = value.toString();
				row.createCell(z).setCellValue(cellValue);
			}
		}
		
		//response输出流导出excel
		try{
			String mimetype = "application/vnd.ms-excel";
			response.setContentType(mimetype);
			response.setCharacterEncoding("UTF-8");
			String fileName = workBookName+".xls";
			response.setHeader("Content-Disposition", "attachment; filename="+URLEncoder.encode(fileName,"UTF-8"));
			out = response.getOutputStream();
			wb.write(out);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 需要合并单元格 特殊的excel导出
	 */
	public void mergeExcelExport(HttpServletResponse response, 
								HSSFWorkbook wb,
								int startRow,
								List<Map<Integer,Object>>cellValues,
								String workBookName){
		OutputStream out=null;
		//获取工作簿中的第一个sheet
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow row;
		for (int i = 0; i < cellValues.size(); i++){
			row = sheet.createRow(startRow++);									//插入一行
			Map<Integer,Object> record = new HashMap<Integer, Object>();
			record = cellValues.get(i);
			for (int j = 0; j < record.size(); j++){							//填充一条记录
				Object value = record.get(j);
				String cellValue = "";
				if (value != null)
					cellValue = value.toString();
				row.createCell(j).setCellValue(cellValue);
			}
		}
		
		//response输出流导出excel
		try{
			String mimetype = "application/vnd.ms-excel";
			response.setContentType(mimetype);
			response.setCharacterEncoding("UTF-8");
			String fileName = workBookName+".xls";
			response.setHeader("Content-Disposition", "attachment; filename="+URLEncoder.encode(fileName,"UTF-8"));
			out = response.getOutputStream();
			wb.write(out);
			out.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	}
	
	/**
	 * 体检表维护导出
	 */
	public void residentCheckIndexExcelExport(HttpServletResponse response,
			java.util.List<Map<Integer,Object>> cellValues,					//列值
			String workBookName){											//工作簿名称
		//表头
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();
		HSSFRow row = sheet.createRow(0);
		HSSFRow row1 = sheet.createRow(1);
		sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
		HSSFCell cell = row.createCell(0);
		cell.setCellValue("序号");
		sheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
		cell = row.createCell(1);
		cell.setCellValue("指标内容");
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 3));
		cell = row.createCell(2);
		cell.setCellValue("参考范围1:");
		cell = row1.createCell(2);
		cell.setCellValue("最小值");
		cell = row1.createCell(3);
		cell.setCellValue("最大值");
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 4, 5));
		cell = row.createCell(4);
		cell.setCellValue("参考范围2:");
		cell = row1.createCell(4);
		cell.setCellValue("最小值");
		cell = row1.createCell(5);
		cell.setCellValue("最大值");
		sheet.addMergedRegion(new CellRangeAddress(0, 1, 6, 6));
		cell = row.createCell(6);
		cell.setCellValue("单位");
		
		mergeExcelExport(response, wb, 2, cellValues, workBookName);
	}
}

